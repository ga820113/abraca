-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主機： localhost
-- 產生時間： 2020 年 08 月 24 日 18:06
-- 伺服器版本： 5.7.29-0ubuntu0.18.04.1
-- PHP 版本： 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `abraca`
--

-- --------------------------------------------------------

--
-- 資料表結構 `Info`
--

CREATE TABLE `Info` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 傾印資料表的資料 `Info`
--

INSERT INTO `Info` (`id`, `name`, `content`) VALUES
(1, 'rule', '規則'),
(2, 'schedule', '進度'),
(3, 'game', '{\"player\":{\"1\":{\"name\":\"\\u73a9\\u5bb61\",\"hp\":6,\"magic\":[6,7,8,8,6],\"secret\":[],\"lv\":1,\"step\":0,\"grade\":0},\"2\":{\"name\":\"\\u73a9\\u5bb62\",\"hp\":6,\"magic\":[8,4,7,7,5],\"secret\":[],\"lv\":1,\"step\":0,\"grade\":0},\"3\":{\"name\":\"\\u73a9\\u5bb63\",\"hp\":6,\"magic\":[3,6,4,8,8],\"secret\":[],\"lv\":1,\"step\":0,\"grade\":0},\"4\":{\"name\":\"\\u73a9\\u5bb64\",\"hp\":6,\"magic\":[7,1,3,7,6],\"secret\":[],\"lv\":1,\"step\":0,\"grade\":0},\"5\":{\"name\":\"\\u73a9\\u5bb65\",\"hp\":6,\"magic\":[8,7,2,6,5],\"secret\":[],\"lv\":1,\"step\":0,\"grade\":0}},\"secret\":[8,5,8,2],\"sort\":[6,5,7,5,3,4,4],\"used\":[],\"round\":[[1,0,0,0]],\"bout\":1,\"last\":0}');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `Info`
--
ALTER TABLE `Info`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `Info`
--
ALTER TABLE `Info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
