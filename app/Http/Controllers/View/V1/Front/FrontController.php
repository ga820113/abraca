<?php

namespace App\Http\Controllers\View\V1\Front;

use App\Http\Controllers\Controller;

class FrontController extends Controller
{
	public function home()
	{
		return view('room');
	}

	public function game($player)
	{
		/*
		if(empty(Session::get('player', NULL))) {
		Session::put('player', $player);
		} else {
		$player = Session::get('player');
		}*/

		return view('game', ['player' => $player]);
	}
}
