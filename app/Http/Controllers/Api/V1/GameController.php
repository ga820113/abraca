<?php

namespace App\Http\Controllers\Api\V1;

use App\Events\GamePush;
use App\Http\Controllers\Controller;
use Cache;
use Illuminate\Http\Request;

class GameController extends Controller
{
	private function get_game_ary()
	{
		$message = Cache::store('redis')->get('game'); //讀取文件

		return json_decode($message, true);
	}

	private function game_reset($bount)
	{
		$game = $this->get_game_ary();

		$player_count = count($game['player']);

		$magic = $this->magic_reset();
		$game['secret'] = $magic['secret']; //秘密符石
		$game['sort'] = $magic['sort']; //剩餘符石
		$game['used'] = []; //已使用符石
		$game['round'] = !empty($game['round']) ? $game['round'] : [[$bount, 0, 0, 0]]; //流程資訊
		$game['bout'] = $bount;
		$game['last'] = 0;

		$sort = $game['sort'];

		if ($player_count == 2) {
			$game['sort'] = array_slice($sort, 12);
			$game['used'] = array_slice($sort, 0, 12);
		} else if ($player_count == 3) {
			$game['sort'] = array_slice($sort, 6);
			$game['used'] = array_slice($sort, 0, 6);
		}

		for ($i = 1; $i <= $player_count; $i++) {
			$sort = $game['sort'];
			$game['player'][$i]['hp'] = 6;
			$game['player'][$i]['magic'] = array_slice($sort, 0, 5);
			$game['player'][$i]['secret'] = [];
			$game['player'][$i]['lv'] = !empty($game['player'][$i]['lv']) ? $game['player'][$i]['lv'] : 1;
			$game['player'][$i]['step'] = 0;
			$game['player'][$i]['grade'] = 0;
			$game['sort'] = array_slice($sort, 5);
		}

		$message = json_encode($game);

		Cache::store('redis')->put('game', $message, 60 * 60);
	}

	//擲骰子
	private function dice()
	{
		return rand(1, 3);
	}

	//初始化魔法符石
	private function magic_reset()
	{
		$all = [1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8];
		shuffle($all);
		$secret = array_slice($all, 0, 4);
		$sort = array_slice($all, 4, 32);

		return ['secret' => $secret, 'sort' => $sort];
	}

	private function last_player($player)
	{
		$game = $this->get_game_ary();
		$player_count = count($game['player']);
		$last_player = $player - 1 != 0 ? $player - 1 : $player_count;
		return $last_player;
	}

	private function next_player($player)
	{
		$game = $this->get_game_ary();
		$player_count = count($game['player']);
		$next_player = $player + 1 > $player_count ? 1 : $player + 1;
		return $next_player;
	}

	private function magic_damage($player, $damage)
	{
		$game = $this->get_game_ary();

		$game['player'][$player]['hp'] += $damage;
		if ($game['player'][$player]['hp'] > 6) {
			$game['player'][$player]['hp'] = 6;
		}
		if ($game['player'][$player]['hp'] < 0) {
			$game['player'][$player]['hp'] = 0;
		}

		return $game['player'][$player]['hp'];
	}

	private function secret_used()
	{
		$game = $this->get_game_ary();
		$secret = [-1];
		foreach ($game['player'] as $key => $value) {
			$secret = array_merge($secret, $game['player'][$key]['secret']);
		}
		return max($secret);
	}

	/************************************************************************************/
	public function create($count)
	{
		$new_game = array();
		for ($i = 1; $i <= $count; $i++) {
			if ($count <= 5) {
				$new_game['player'][$i]['name'] = '玩家' . $i;
			}
		}

		$message = json_encode($new_game);
		Cache::store('redis')->put('game', $message, 60 * 60);

		$this->game_reset(1);
	}

	public function load()
	{
		$message = Cache::store('redis')->get('game'); //讀取文件

		return $message;
	}

	public function magic(Request $request)
	{
		$game = $this->get_game_ary();

		$player = (int) $request->player;
		$no = (int) $request->no;

		if ($game['bout'] == $player) {
			$success = 0;
			$dice = $no == 1 || $no == 3 ? $this->dice() : 0;
			$player_count = count($game['player']);

			if ($no != 0) {
				if ($game['last'] > $no) {
					return 2;
				}
				$player_magic = $game['player'][$player]['magic'];
				if (in_array($no, $player_magic)) {
					$success = 1;
				}
				array_unshift($game['round'], [$player, $no, $success, $dice]); //玩家，施展魔法，成功/失敗，骰子點數
				$mistake = 0; //自殺
				$defeat = 0; //擊殺

				/********************************************************/
				//施展魔法
				if ($success == 0) {
					$damage = $no == 1 ? -$dice : -1;
					$game['player'][$player]['hp'] = $this->magic_damage($player, $damage);

					if ($game['player'][$player]['hp'] == 0) {
						$mistake = 1;
					} else {
						$bout = $this->next_player($player);
						array_unshift($game['round'], [$bout, 0, 0, 0]);
						$game['bout'] = $bout;
					}

					$sort = $game['sort']; //剩餘魔法符石
					$remainder = count($player_magic);
					if ($remainder < 5) {
						$plus = 5 - $remainder;
						if (count($sort) != 0) {
							$plus = $plus > count($sort) ? count($sort) : $plus; //可抽數量
							$magic_plus = array_splice($sort, 0, $plus);
							$game['player'][$player]['magic'] = array_merge($game['player'][$player]['magic'], $magic_plus);
							$game['sort'] = array_splice($game['sort'], $plus, -1);
						}
					}
					$game['last'] = 0;
				} else {
					$damage = 0;
					$magic_use = array_search($no, $game['player'][$player]['magic']);
					$used = array_splice($game['player'][$player]['magic'], $magic_use, 1);

					$game['used'] = array_merge($game['used'], $used);
					$game['last'] = $no;

					switch ($no) {
						case 1:
							$damage = -$dice;
							foreach ($game['player'] as $key => $value) {
								if ($key != $player) {
									$game['player'][$key]['hp'] = $this->magic_damage($key, $damage);
									if ($game['player'][$key]['hp'] == 0) {
										$defeat++;
									}
								}
							}
							break;
						case 2:
							foreach ($game['player'] as $key => $value) {
								if ($key != $player) {
									$game['player'][$key]['hp'] = $this->magic_damage($key, -1);
									if ($game['player'][$key]['hp'] == 0) {
										$defeat++;
									}
								} else {
									$game['player'][$key]['hp'] = $this->magic_damage($key, 1);
								}
							}
							break;
						case 3:
							$game['player'][$player]['hp'] = $this->magic_damage($player, $dice);
							break;
						case 4:
							array_push($game['player'][$player]['secret'], $this->secret_used() + 1);
							break;
						case 5:
							$last_player = $this->last_player($player);
							$next_player = $this->next_player($player);
							if ($last_player != $next_player) {
								$game['player'][$last_player]['hp'] = $this->magic_damage($last_player, -1);
								$game['player'][$next_player]['hp'] = $this->magic_damage($next_player, -1);
								if ($game['player'][$last_player]['hp'] == 0) {
									$defeat++;
								}
								if ($game['player'][$next_player]['hp'] == 0) {
									$defeat++;
								}
							} else {
								$game['player'][$last_player]['hp'] = $this->magic_damage($last_player, -1);
								if ($game['player'][$last_player]['hp'] == 0) {
									$defeat++;
								}
							}
							break;
						case 6:
							$last_player = $this->last_player($player);
							$game['player'][$last_player]['hp'] = $this->magic_damage($last_player, -1);
							if ($game['player'][$last_player]['hp'] == 0) {
								$defeat++;
							}
							break;
						case 7:
							$next_player = $this->next_player($player);
							$game['player'][$next_player]['hp'] = $this->magic_damage($next_player, -1);
							if ($game['player'][$next_player]['hp'] == 0) {
								$defeat++;
							}
							break;
						case 8:
							$game['player'][$player]['hp'] = $this->magic_damage($player, 1);
							break;
					}
				}

				//回合結束
				$end = 0;
				if ($defeat > 0) {
					//擊殺
					foreach ($game['player'] as $key => $value) {
						if ($value['hp'] != 0) {
							$step = count($game['player'][$key]['secret']);
							if ($key == $player) {
								$step += 3;
							} else {
								$step += 1;
							}
							$game['player'][$key]['lv'] = $game['player'][$key]['lv'] + $step > 8 ? 8 : $game['player'][$key]['lv'] + $step;
							$game['player'][$key]['step'] = $step;
						}
					}
					$end = $player;
				} else if ($mistake > 0) {
					//自殺
					foreach ($game['player'] as $key => $value) {
						if ($value['hp'] != 0) {
							$step = 1 + count($game['player'][$key]['secret']);
							$game['player'][$key]['lv'] = $game['player'][$key]['lv'] + $step > 8 ? 8 : $game['player'][$key]['lv'] + $step;
							$game['player'][$key]['step'] = $step;
						}
					}
					$end = $this->next_player($player);
				} else if (count($game['player'][$player]['magic']) == 0) {
					//使用完符石
					foreach ($game['player'] as $key => $value) {
						$step = count($game['player'][$key]['secret']);
						if ($key == $player) {
							$step += 3;
						}
						$game['player'][$key]['lv'] = $game['player'][$key]['lv'] + $step > 8 ? 8 : $game['player'][$key]['lv'] + $step;
						$game['player'][$key]['step'] = $step;
					}
					$end = $player;
				}

				//是否有人爬到第8層
				$winner = [];
				if ($end != 0) {
					foreach ($game['player'] as $key => $value) {
						if ($value['lv'] >= 8) {
							array_push($winner, $key);
						}
					}
					if (count($winner) > 0) {
						$max = 0;
						foreach ($winner as $value) {
							$game['player'][$value]['grade'] = $game['player'][$value]['step'] * 10 + $game['player'][$value]['hp'];
							if ($game['player'][$value]['grade'] > $max) {
								$max = $game['player'][$value]['grade'];
							}
						}
						foreach ($winner as $key => $value) {
							if ($game['player'][$value]['grade'] < $max) {
								unset($winner[$key]);
							}
						}
						array_unshift($game['round'], ['w', $winner, 0, 0]); //勝利名單
					} else {
						array_unshift($game['round'], ['n', 0, 0, 0]); //新的回合
						array_unshift($game['round'], [$end, 0, 0, 0]);

						Cache::store('redis')->put('game', $message, 60 * 60);
						$message = json_encode($game);

						return $this->game_reset($end);
					}
				}
				/********************************************************/
			} else {
				if ($game['round'][0][0] == $player && $game['round'][0][1] == 0) {
					return 3;
				}

				$player_magic = $game['player'][$player]['magic'];
				$sort = $game['sort']; //剩餘魔法符石
				$remainder = count($player_magic);
				if ($remainder < 5) {
					$plus = 5 - $remainder;
					if (count($sort) != 0) {
						$plus = $plus > count($sort) ? count($sort) : $plus; //可抽數量
						$magic_plus = array_splice($sort, 0, $plus);
						$game['player'][$player]['magic'] = array_merge($game['player'][$player]['magic'], $magic_plus);
						$game['sort'] = array_splice($game['sort'], $plus, -1);
					}
				}

				$player = $this->next_player($player);
				array_unshift($game['round'], [$player, 0, 0, 0]);
				$game['bout'] = $player;
				$game['last'] = 0;
			}

			Cache::store('redis')->put('game', $message, 60 * 60);
			$message = json_encode($game);

			return 1;
		} else {
			return 0;
		}
	}

	public function push()
	{
		event(new GamePush());

		return true;
	}
}
