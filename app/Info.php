<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    protected $table = 'Info';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
