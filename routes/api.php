<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['namespace' => 'Api', 'as' => 'api::'], function () {
    Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {
        Route::group(['prefix' => 'game'], function () {
            Route::get('/create/{count}', 'GameController@create')->name('create');
            Route::get('/load', 'GameController@load');
            Route::post('/magic', 'GameController@magic');
            Route::get('/push', 'GameController@push');
        });
    });
});
