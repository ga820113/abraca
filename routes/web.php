<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['namespace' => 'View'], function () {
    Route::group(['namespace' => 'V1'], function () {
        Route::group(['namespace' => 'Front', 'as' => 'UserV1::Front::'], function () {
            Route::get('/', 'FrontController@home')->name('home');
            Route::get('/{player?}', 'FrontController@game')->name('game');
        });
    });
});
