@extends('main')

@section('content')
<div class="room">
    <div class="room-list">
        <center>
            <h2><b>出包魔法師</b></h2>
        </center>
        <div class="room-item half">
            <input type="number" min="2" max="5" placeholder="人數2~5" id="player">
            <btnnon class="btn btn-default" onclick="add()">建立</btnnon>
        </div>
        <div class="room-item half">
            <input type="number" min="1" max="5" placeholder="玩家編號1~5" id="no">
            <btnnon class="btn btn-default" onclick="join()">加入</btnnon>
        </div>
        <div class="room-item full">
            <a href="{{ route('UserV1::Front::game', ['player' => 0])}}" class="btn btn-default">觀戰</a>
        </div>
        <div class="room-item full">
            <button class="btn btn-default" onclick="expand('rule')">遊戲說明</button>
        </div>
        <div class="room-item full" style="padding: 5px;color: red;">
            當玩家編號超過遊戲設定人數時，將自動成為觀戰者
        </div>
    </div>
</div>
<div class="expand" style="display: none;">
    <div class="expand-item rule">
        <div class="expand-close">
            <button class="btn btn-default" onclick="expand_close()">關閉</button>
        </div>
        <h3><b>說明</b>:</h3>
        <h4>玩家輪流盡可能地施展魔法，先行使用完魔法或使其他人HP歸零，從對手的魔法符石及已使用的魔法來推敲手上的魔法吧!</h4>
        <hr>
        <h3><b>規則</b>:</h3>
        <h4>一.法術連鎖到施法失敗、停止施法、用完法術，法術施放只能大於或等於前次法術</h4>
        <h4>二.施法連鎖結束後補充到5塊符石，除非已沒有剩餘魔法符石</h4>
        <h4>三.2/3人遊玩時要移除12/6塊符石</h4>
        <h4>四.施法失敗除了巨龍以外損失1點生命</h4>
        <h4>五.每輪會有4塊秘密符石</h4>
        <hr>
        <h3><b>回合結束</b>:</h3>
        <h4>有玩家擊殺其他玩家，擊殺者前進3步，存活者前進1步</h4>
        <h4>有玩家用完符石，前進3步</h4>
        <h4>有玩家自殺，存活者前進1步</h4>
        <hr>
        <h3><b>勝利</b>:</h3>
        <h3>抵達到第8層為勝利者</h3>
        <h4>同時抵達者以該局上升最大者獲勝</h4>
        <h4>依然平手時以生命點數判定</h4>
        <h4>依然平手時同時勝利</h4>
        <hr>
        <h3><b>魔法符石</b>:</h3>
        <table>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/1.png"></td>
                <td width="80%">
                    <h4>1.古代巨龍:<br>擲骰子所有對手損失骰子點數，召喚失敗損失骰子點數數量的生命值</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/2.png"></td>
                <td width="80%">
                    <h4>2.黑暗幽靈:<br>所有對手減1點生命，自己回復1點</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/3.png"></td>
                <td width="80%">
                    <h4>3.甜蜜的夢:<br>回復骰子點數的生命值</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/4.png"></td>
                <td width="80%">
                    <h4>4.貓頭鷹:<br>取得一塊秘密符石，遊戲結束後存活可多得一分</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/5.png"></td>
                <td width="80%">
                    <h4>5.閃電暴風雨:<br>上下玩家各損失1點生命，同玩家只會損失1次</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/6.png"></td>
                <td width="80%">
                    <h4>6.暴風雪:<br>上方玩家損失1點生命</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/7.png"></td>
                <td width="80%">
                    <h4>7.火球術:<br>下方玩家損失1點生命</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/8.png"></td>
                <td width="80%">
                    <h4>8.魔法藥水:<br>回復1點生命</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/0.png"></td>
                <td width="80%">
                    <h4>停止施法:<br>至少施法成功一次才可使用，停止自己的回合</h4>
                </td>
            </tr>
        </table>
    </div>
    <div class="expand-exit"></div>
</div>
@endsection

@section('script')
<script>
function add() {
    var player = $('#player').val();
    var url = "/api/v1/game/create/" + player;
    $.ajax({
        url: url,
        method: 'get',
        success: function() {
            alert('建立' + player + '人遊戲');
        }
    });
}

function join() {
    var no = $('#no').val();
    var url = "{{ route('UserV1::Front::game', ['player' => ''])}}/" + no;
    location.href = url;
}
</script>
@endsection