<!DOCTYPE html>
<html>

<head>
    <title>出包魔法師</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="auth-token" content="{{ session('token') }}">
    @include('library')
</head>

<body>
    @section('content')
    @show

    @section('script')
    @show
</body>

</html>