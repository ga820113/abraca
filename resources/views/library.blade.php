<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/game.js') }}"></script>
<!-- pusher -->
<script src="https://js.pusher.com/6.0/pusher.min.js"></script>
<!--theme-style-->
<link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
<link href="{{ asset('css/game.css') }}" rel="stylesheet" type="text/css" media="all" />

<link rel="icon" sizes="192x192" href="{{ asset('image/icon.jpg') }}">
<link rel="icon" sizes="128x128" href="{{ asset('image/icon.jpg') }}">