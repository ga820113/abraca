@extends('main')

@section('content')
<div class="top">
    <div class="top-list">
        <button class="btn btn-default" onclick="expand('rule')">遊戲說明</button>
        <button class="btn btn-default" onclick="expand('used_magic')">魔法符石</button>
        <button class="btn btn-default" onclick="expand('level')">勝利進度</button>
    </div>
</div>
<div class="content">
    <div class="notice">
        <div class="">
            <h4>流程</h4>
            <div class="status" style="height: 150px;overflow: auto;"></div>
        </div>
    </div>
    <div class="player">
        <div class="magic-list">
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(1)">
                    <img src="{{ asset('image') }}/1.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(2)">
                    <img src="{{ asset('image') }}/2.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(3)">
                    <img src="{{ asset('image') }}/3.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(4)">
                    <img src="{{ asset('image') }}/4.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(5)">
                    <img src="{{ asset('image') }}/5.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(6)">
                    <img src="{{ asset('image') }}/6.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(7)">
                    <img src="{{ asset('image') }}/7.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(8)">
                    <img src="{{ asset('image') }}/8.png">
                </button>
            </div>
            <div class="magic-item">
                <button class="btn-default magic" onclick="magic(0)">
                    <img src="{{ asset('image') }}/0.png">
                </button>
            </div>
        </div>
        <div class="player-list">
            <div class="player-item p1" style="display: none;">
                <div class='player-info'>
                    <span class='p1_name'></span><br>
                    <div class='p1_hp'></div>
                </div>
                <div class='player-magic p1_magic'></div>
            </div>

            <div class="player-item p2" style="display: none;">
                <div class='player-info'>
                    <span class='p2_name'></span><br>
                    <div class='p2_hp'></div>
                </div>
                <div class='player-magic p2_magic'></div>
            </div>
            <div class="player-item p3" style="display: none;">
                <div class='player-info'>
                    <span class='p3_name'></span><br>
                    <div class='p3_hp'></div>
                </div>
                <div class='player-magic p3_magic'></div>
            </div>
            <div class="player-item p4" style="display: none;">
                <div class='player-info'>
                    <span class='p4_name'></span><br>
                    <div class='p4_hp'></div>
                </div>
                <div class='player-magic p4_magic'></div>
            </div>
            <div class="player-item p5" style="display: none;">
                <div class='player-info'>
                    <span class='p5_name'></span><br>
                    <div class='p5_hp'></div>
                </div>
                <div class='player-magic p5_magic'></div>
            </div>

        </div>
    </div>
</div>
<div class="expand" style="display: none;">
    <div class="expand-item rule">
        <div class="expand-close">
            <button class="btn btn-default" onclick="expand_close()">關閉</button>
        </div>
        <h3><b>說明</b>:</h3>
        <h4>玩家輪流盡可能地施展魔法，先行使用完魔法或使其他人HP歸零，從對手的魔法符石及已使用的魔法來推敲手上的魔法吧!</h4>
        <hr>
        <h3><b>規則</b>:</h3>
        <h4>一.法術連鎖到施法失敗、停止施法、用完法術，法術施放只能大於或等於前次法術</h4>
        <h4>二.施法連鎖結束後補充到5塊符石，除非已沒有剩餘魔法符石</h4>
        <h4>三.2/3人遊玩時要移除12/6塊符石</h4>
        <h4>四.施法失敗除了巨龍以外損失1點生命</h4>
        <h4>五.每輪會有4塊秘密符石</h4>
        <hr>
        <h3><b>回合結束</b>:</h3>
        <h4>有玩家擊殺其他玩家，擊殺者前進3步，存活者前進1步</h4>
        <h4>有玩家用完符石，前進3步</h4>
        <h4>有玩家自殺，存活者前進1步</h4>
        <hr>
        <h3><b>勝利</b>:</h3>
        <h3>抵達到第8層為勝利者</h3>
        <h4>同時抵達者以該局上升最大者獲勝</h4>
        <h4>依然平手時以生命點數判定</h4>
        <h4>依然平手時同時勝利</h4>
        <hr>
        <h3><b>魔法符石</b>:</h3>
        <table>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/1.png"></td>
                <td width="80%">
                    <h4>1.古代巨龍:<br>擲骰子所有對手損失骰子點數，召喚失敗損失骰子點數數量的生命值</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/2.png"></td>
                <td width="80%">
                    <h4>2.黑暗幽靈:<br>所有對手減1點生命，自己回復1點</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/3.png"></td>
                <td width="80%">
                    <h4>3.甜蜜的夢:<br>回復骰子點數的生命值</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/4.png"></td>
                <td width="80%">
                    <h4>4.貓頭鷹:<br>取得一塊秘密符石，遊戲結束後存活可多得一分</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/5.png"></td>
                <td width="80%">
                    <h4>5.閃電暴風雨:<br>上下玩家各損失1點生命，同玩家只會損失1次</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/6.png"></td>
                <td width="80%">
                    <h4>6.暴風雪:<br>上方玩家損失1點生命</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/7.png"></td>
                <td width="80%">
                    <h4>7.火球術:<br>下方玩家損失1點生命</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/8.png"></td>
                <td width="80%">
                    <h4>8.魔法藥水:<br>回復1點生命</h4>
                </td>
            </tr>
            <tr>
                <td width="20%"><img src="{{ asset('image') }}/0.png"></td>
                <td width="80%">
                    <h4>停止施法:<br>至少施法成功一次才可使用，停止自己的回合</h4>
                </td>
            </tr>
        </table>
    </div>
    <div class="expand-item level">
        <div class="expand-close">
            <button class="btn btn-default" onclick="expand_close()">關閉</button>
        </div>
        <div>第8層:<span id='lv8'></span></div>
        <div>第7層:<span id='lv7'></span></div>
        <div>第6層:<span id='lv6'></span></div>
        <div>第5層:<span id='lv5'></span></div>
        <div>第4層:<span id='lv4'></span></div>
        <div>第3層:<span id='lv3'></span></div>
        <div>第2層:<span id='lv2'></span></div>
        <div>第1層:<span id='lv1'></span></div>
    </div>
    <div class="expand-item used_magic">
        <div class="expand-close">
            <button class="btn btn-default" onclick="expand_close()">關閉</button>
        </div>
        已使用魔法符石
        <div class="magic_1"></div>
        <div class="magic_2"></div>
        <div class="magic_3"></div>
        <div class="magic_4"></div>
        <div class="magic_5"></div>
        <div class="magic_6"></div>
        <div class="magic_7"></div>
        <div class="magic_8"></div>
        <hr>
        剩餘魔法符石
        <div class="magic_0"></div>
        <hr>
        <div class='secret'></div>
    </div>
    <div class="expand-exit"></div>
</div>
@endsection

@section('script')
<script type="text/javascript">
// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('6425b34b039bf502ce15', {
    cluster: 'eu'
});

var channel = pusher.subscribe('my-channel');
channel.bind('game-push', function(data) {
    load();
});

load();

function load() {
    var result = null;
    $.ajax({
        url: "/api/v1/game/load",
        method: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
            $('.magic').attr('disabled', false);
        }
    });

    player_magic_set(result); //卡牌配置
    status_set(result); //流程讀取
    used_set(result); //已使用符石讀取
}

function player_magic_set(result) {
    var player = result['player'];
    var secret = result['secret'];

    $("span[id^='lv']").html('');

    for (var key in player) {
        $('.p' + key).show();
        $('.p' + key + '_name').html(player[key]['name']);
        $('.p' + key + '_hp').html(`<img src="{{ asset('image') }}/p${key}.png" style="width: 75%;"><img src="{{ asset('image') }}/hp-${player[key]['hp']}.jpg" style="width: 25%;">`);


        var lv = player[key]['lv'];
        $('#lv' + lv).append(player[key]['name'] + ' ');

        var magic_img = '';
        var secret_img = '秘密符石<br>';
        if (key == '{{ $player }}') {
            var magic = player[key]['magic'];
            for (var i = 0; i < magic.length; i++) {
                magic_img += `<img src="{{ asset('image') }}/mine.png" style="width: 15%;">`;
            }
            $('.p' + key + '_magic').html(magic_img);

            for (var i = 0; i < secret.length; i++) {
                if (player[key]['secret'].indexOf(i) >= 0) {
                    secret_img += `<img src="{{ asset('image') }}/${secret[i]}.png" style="width: 20%;">`;
                } else {
                    secret_img += `<img src="{{ asset('image') }}/mine.png" style="width: 20%;">`;
                }
            }
            $('.secret').html(secret_img);
        } else {
            var magic = player[key]['magic'];
            for (var i = 0; i < magic.length; i++) {
                magic_img += `<img src="{{ asset('image') }}/${magic[i]}.png" style="width: 15%;">`;
            }
            $('.p' + key + '_magic').html(magic_img);
        }
    }
}

function status_set(result) {
    var player = result['player'];
    var round = result['round'];
    var status = '';

    for (var i = 0; i < round.length; i++) {
        var magic = '';
        var success = '';
        var enemy = '';

        if (round[i][2] == 1) {
            success = '成功';
        } else {
            success = '失敗';
            enemy = ',損失了1點hp';
        }
        switch (round[i][1]) {
            case 1:
                magic = '古代巨龍';
                if (round[i][2] == 1) {
                    enemy = ',所有對手損失' + round[i][3] + 'hp';
                } else {
                    enemy = ',自己損失' + round[i][3] + 'hp';
                }
                break;
            case 2:
                magic = '黑暗幽靈';
                break;
            case 3:
                magic = '甜蜜的夢';
                if (round[i][2] == 1) {
                    enemy = ',自己回復' + round[i][3] + '點hp';
                }
                break;
            case 4:
                magic = '貓頭鷹';
                break;
            case 5:
                magic = '閃電暴風雨';
                break;
            case 6:
                magic = '暴風雪';
                break;
            case 7:
                magic = '火球術';
                break;
            case 8:
                magic = '魔法藥水';
                break;
        }

        if (round[i][0] == 'w') {
            var list = '';
            for (var j = 0; j < round[i][1].length; j++) {
                if (j == 0) {
                    list += player[round[i][1][j]]['name'];
                } else {
                    list += ',' + player[round[i][1][j]]['name'];
                }
            }
            status += list + '贏得了勝利<br>';
            $('.magic').attr('disabled', true);
            break;
        } else if (round[i][0] == 'n') {
            status += '本局結束，結算後開啟新局<br>';
        } else if (round[i][1] != 0) {
            status += player[round[i][0]]['name'] + '使用' + magic + success + enemy + '<br>';
        } else {
            status += player[round[i][0]]['name'] + '的回合<br>';
        }
    }
    $('.status').html(status);
}

function used_set(result) {
    var used = result['used'];
    var sort = result['sort'];

    var magic_0 = "";
    var magic_1 = "<img src='{{ asset('image') }}/1.png'>";
    var magic_2 = "<img src='{{ asset('image') }}/2.png'>";
    var magic_3 = "<img src='{{ asset('image') }}/3.png'>";
    var magic_4 = "<img src='{{ asset('image') }}/4.png'>";
    var magic_5 = "<img src='{{ asset('image') }}/5.png'>";
    var magic_6 = "<img src='{{ asset('image') }}/6.png'>";
    var magic_7 = "<img src='{{ asset('image') }}/7.png'>";
    var magic_8 = "<img src='{{ asset('image') }}/8.png'>";

    for (var i = 0; i < sort.length; i++) {
        magic_0 += "<img src='{{ asset('image') }}/mine.png'>";
    }

    for (var i = 0; i < used.length; i++) {
        switch (used[i]) {
            case 1:
                magic_1 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 2:
                magic_2 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 3:
                magic_3 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 4:
                magic_4 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 5:
                magic_5 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 6:
                magic_6 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 7:
                magic_7 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
            case 8:
                magic_8 += "<img src='{{ asset('image') }}/mine.png'>"
                break;
        }
    }
    $('.magic_0').html(magic_0);
    $('.magic_1').html(magic_1);
    $('.magic_2').html(magic_2);
    $('.magic_3').html(magic_3);
    $('.magic_4').html(magic_4);
    $('.magic_5').html(magic_5);
    $('.magic_6').html(magic_6);
    $('.magic_7').html(magic_7);
    $('.magic_8').html(magic_8);
}

function magic(no) {
    var result = null;
    $('.magic').attr('disabled', true);
    $.ajax({
        url: "/api/v1/game/magic",
        method: 'post',
        data: {
            no: no,
            player: '{{ $player }}'
        },
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });

    if (result == 0) {
        alert('還沒輪到你!');
    } else if (result == 3) {
        alert('至少要施放一次法術');
        $('.magic').attr('disabled', false);
    } else if (result == 2) {
        alert('法術施放只能大於或等於前次法術');
        $('.magic').attr('disabled', false);
    } else {
        push();
    }
}

function push() {
    var result = null;
    $.ajax({
        url: "/api/v1/game/push",
        method: 'get',
        dataType: 'json',
        async: false,
        success: function(data) {
            result = data;
        }
    });
}
</script>
@endsection