$().ready(function (){
	$('.expand-exit').on('click', function() {
		$('.expand').hide();
		$('.expand-item').hide();
	});
});

function expand(event) {
	$('.'+event).show();
	$('.expand').show();
}

function expand_close() {
	$('.expand').hide();
	$('.expand-item').hide();
}